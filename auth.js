const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code that can decode the encrypted information
*/


//Token Creation
module.exports.createAccessToken = (user) => {
	// When the user logs in, a token will be created with user's information

	// {
	//   _id: new ObjectId("63dc65ad54cd7478899ffb37"),
	//   firstName: 'John',
	//   lastName: 'Smith',
	//   email: 'john@mail.com',
	//   password: '$2b$10$dZ1SxxY/PPyK3J.2qhbVL.dAj1lo/YK6z0cnjri0d7NLGnJ3rwwUu',
	//   isAdmin: false,
	//   mobileNo: '09123456789',
	//   enrollments: [],
	//   __v: 0
	// }

	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	//jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});


}


//Token Verification

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token

	console.log(req.headers)
	  // 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWFkNTRjZDc0Nzg4OTlmZmIzNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MDl9.wcvAs648tjoGCIOJYBk51ZTt4U03PV1z7rqco3TpfrY'

	let token = req.headers.authorization;

	// Token received and is not undefined
	if(typeof token !== "undefined") {
		console.log(token);

		// The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification
		token = token.slice(7, token.length);

		// 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWFkNTRjZDc0Nzg4OTlmZmIzNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MDl9.wcvAs648tjoGCIOJYBk51ZTt4U03PV1z7rqco3TpfrY'

		// Validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (error, data) => {

				// If JWT is not valid
				if(error) {
					return res.send({auth: "failed"});

				// If JWT is valid
				} else {

					// Allows the application to proceed with the next middleware function/callback function in the route
					// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
					next();
				}
		})

	// Token does not exist
	} else {
		return res.send({auth: "failed" });
	}
}


//Token Decryption
module.exports.decode = (token) => {

	 // 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWFkNTRjZDc0Nzg4OTlmZmIzNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MDl9.wcvAs648tjoGCIOJYBk51ZTt4U03PV1z7rqco3TpfrY'

	// Token recieved and is not undefined
	if (typeof token !== "undefined") {

		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		//'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWFkNTRjZDc0Nzg4OTlmZmIzNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MDl9.wcvAs648tjoGCIOJYBk51ZTt4U03PV1z7rqco3TpfrY'


		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return null
			} else {

				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	// Token does not exist
	} else {
		return null

	}
}