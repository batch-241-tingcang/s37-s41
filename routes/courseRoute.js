const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


//Route for creating a course

// router.post("/", (req, res) => {

// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

// });

router.post("/addCourse", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Activity
// router.post("/addCourse", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization)

// 	if(!userData.isAdmin) {
// 		return res.status(401).send({error: "Only admin users can create courses."})
// 	} else {
// 		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// 	}

// });

// Route for retrieving all the courses.
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Retrieve all the ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Archive a course (ACTIVITY)
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});





module.exports = router;