const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


//Route for User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	/*
		{
		  id: '63dc65ad54cd7478899ffb37',
		  email: 'john@mail.com',
		  isAdmin: false,
		  iat: 1675402509
		}

	*/

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route to enroll a user to a course
// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	};
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// });

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
















