const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.addCourse = (data) => {
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		});

	};

		let message = Promise.resolve("User must be ADMIN to access this.")
		return message.then((value) => {
			return {value};
		})
	
};


// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve ACTIVE COURSES
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqparams) => {
	return Course.findById(reqparams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams,reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		SYNTAX:
		findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
	});

};

// Archive a course (ACTIVITY)
/*
	module.exports.archiveCourse = (reqParams, reqBody) => {
	return Course.findOneAndUpdate({_id:reqParams.courseId},{ isActive: false },{returnOriginal: true}).then(result => {
		console.log(result);
		if(result.isActive) {
			return false;
	}	else {
			return true;
	}

	});
};
*/

// Archive a course
module.exports.archiveCourse = (reqParams,reqBody) => {
	let updateActiveField = {
		isActive: false
	};

	/*
		SYNTAX:
		findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
	});

};







